//  Khai báo thư viện express
const express = require('express');

// Import
const { createUser, getAlluser, getUserById, updateUserById, deleteUserById } = require('../controller/usercontroller');


//Tạo router
const userRouter = express.Router();

//CREATE A USER
userRouter.post('/users', createUser);


//GET ALL USER
userRouter.get('/users', getAlluser);


//GET A USER
userRouter.get('/users/:userId', getUserById)


//UPDATE A USER
userRouter.put('/users/:userId', updateUserById)


//DELETE A USER
userRouter.delete('/users/:userId', deleteUserById)

module.exports = userRouter