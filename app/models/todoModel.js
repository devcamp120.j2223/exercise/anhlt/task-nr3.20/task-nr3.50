//B1: khai báo sử dụng mongoose
const mongoose = require("mongoose");

//B2: khai báo schema
const Schema = mongoose.Schema;

//B3: khơi tạo 1 schema
const todosSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    userId : {
        type : mongoose.Types.ObjectId,
        ref : 'user'
    },
    title : {
            type : String,
        required : true    
    },
    completed : {
        type: String,
        required : true 
    }
});

//B4: export model compile từ schema
module.exports = mongoose.model("todos", todosSchema);