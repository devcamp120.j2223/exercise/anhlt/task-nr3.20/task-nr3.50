//B1: khai báo sử dụng mongoose
const mongoose = require("mongoose");

//B2: khai báo schema
const Schema = mongoose.Schema;

//B3: khơi tạo 1 schema
const potoSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    albumId : {
        type : mongoose.Types.ObjectId,
        ref : 'album'
    },
    title : {
            type : String,
        required : true    
    },
    url : {
        type: String,
        required : true 
    },
    thumbnailUrl : {
        type: String,
        required : true 
    }
});

//B4: export model compile từ schema
module.exports = mongoose.model("poto", potoSchema);